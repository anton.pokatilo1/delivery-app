using DeliveryApp.Api.Logic;

namespace DeliveryApp.UnitTests;

public class SimpleServiceShould
{
    [Theory]
    [InlineData(1, 1, 2)]
    [InlineData(1, 2, 3)]
    [InlineData(2, 3, 5)]
    [InlineData(3, 5, 8)]
    public void ReturnCorrectSum(int a, int b, int expectedSum)
    {
        //Arrange
        var service = new SimpleService();

        //Act
        var actualSum = service.GetSum(a, b);

        //Assert
        Assert.Equal(expectedSum, actualSum);
    }
}