namespace DeliveryApp.Api.Logic;

public class SimpleService : ISimpleService
{
    public int GetSum(int a, int b)
    {
        return a + b;
    }
}
