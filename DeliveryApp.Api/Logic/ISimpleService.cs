namespace DeliveryApp.Api.Logic;
public interface ISimpleService
{
    int GetSum(int a, int b);
}